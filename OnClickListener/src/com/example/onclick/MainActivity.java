package com.example.onclick;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity {

	private TextView tv1;
	private Button btn1;
	private TextView tv2;
	private Button longBtn;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		tv1 = (TextView) findViewById(R.id.textView1);
		btn1 = (Button) findViewById(R.id.button1);
		btn1.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				tv1.setText("눌렀다.");
			}
		});
		tv2 = (TextView) findViewById(R.id.textView2);
		longBtn = (Button) findViewById(R.id.longbutton);
		longBtn.setOnLongClickListener(new View.OnLongClickListener() {
			
			@Override
			public boolean onLongClick(View v) {
				tv2.setText("길게눌렀다.");
				return false;
			}
		});
	}
}
